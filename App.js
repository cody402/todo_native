import React, { Component, Fragment } from 'react'
import { Text, View, FlatList, TextInput, ScrollView, AppState, StatusBar, Platform, ToolbarAndroid, Button } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import MainStyles from './styles/MainStyles'
import Task from './components/task'
import { AsyncStorage } from "react-native"

export default class App extends Component {
  state = {
    tasks: [],
    text: ''
  };

  componentDidMount() {
    this.setState({ text: '', tasks: [] })
    AppState.addEventListener('change', this.handleAppStateChange);
    this.retrieveData()  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  RenderTaskItem = item =>{
    return <Task item={item} checked={item.checked} deleteTask={this.deleteTask} editTask={this.editTask}/>
  }

  render () {
    return(
      <Fragment>
        <StatusBar translucent backgroundColor="rgba(0, 0, 0, 0.24)" animated/>
        <LinearGradient style ={{flex: 1} }colors={['#6699ff', '#ff9999']}>
        <View style={MainStyles.container}>
          <View style={MainStyles.header}>
            <Text style={MainStyles.headerText}>Todo</Text>
            <Text style={MainStyles.headerText}>{this.state.tasks.filter(task => task.checked).length}/{this.state.tasks.length}</Text>
            <Button
            onPress={() => this.resetTaskList()}
            title='Reset'></Button>
          </View>
            <FlatList
              data={this.state.tasks}
              renderItem={({item}) => this.RenderTaskItem(item)}
              keyExtractor={item => item.key.toString()}
              ItemSeparatorComponent ={() => <View style={MainStyles.separator}/>}
              />
            <TextInput
              style={MainStyles.textInput}
              onSubmitEditing={this.addTask}
              onChangeText={this.changeTextHandler}
              value={this.state.text}
              placeholder="Add Task"
            />
        </View>
      </LinearGradient>
    </Fragment>
    )
  }

  //Tasks
  changeTextHandler = text => {
    this.setState({ text })
  };

  addTask = () => {
    this.setState(
      prevState => {
        let { tasks, text} = prevState;
        if(text.length > 0){
          return {
            tasks: tasks.concat({ key: tasks.length, name: text }),
          };
        }
      },
    );
    this.setState({ text: '' })
  }

  editTask = (key, text, checked) => {
    this.setState(
      prevState => {
        let tasks = prevState.tasks
        tasks.forEach(element => {
          if(element.key === key){
            element.name = text
            element.checked = checked
          }
        });
        return {
          tasks,
        };
      },
    );
  }

  deleteTask = i => {
    this.setState(
      prevState => {
        let tasks = prevState.tasks
        return { tasks: tasks.filter((task) => { return task.key != i }) }
      },
    );
  };

  resetTaskList = () => {
    this.setState({tasks: []})
  }

  //Local Storage
  handleAppStateChange = (nextAppState) => {
    if (nextAppState === 'inactive' || nextAppState === 'background') {
      console.log('test')
      this.storeData()
    }
  }
  storeData = async () => {
    console.log('should save')
    try {
        await AsyncStorage.setItem('tasks', JSON.stringify(this.state.tasks));
    } catch (error) {
        console.log(error)
    }
  }

  retrieveData = async () => {
    try {
        const value = JSON.parse(await AsyncStorage.getItem('tasks'))
        console.log(value)
        if (value !== null) {
            this.setState({tasks: value})
        }
    } catch (error) {
      console.log(error)
    }
  }
}
