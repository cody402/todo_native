import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    container: {
      flex: 1,
    },
    header: {
      backgroundColor: '#4c73c2',
      padding: 10,
      paddingTop: 40,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    card: {
      backgroundColor: 'rgba(255, 255,255, 0.7)',
      padding: 15,
    },
    headerText: {
      fontSize: 20,
      color: '#fff',
    },
    taskText: {
      fontSize: 20,
      textAlign: 'center',
    },
    checked: {
      backgroundColor: '#b6ff99',
    },
    textInput: {
      height: 50,
      backgroundColor: '#fff',
      paddingRight: 10,
      paddingLeft: 10,
      borderColor: "gray",
      width: '100%'
    },
    separator: {
      height: 2,
      width: "100%",
      backgroundColor: "#a6a6a6",
    }
  });
