import React from 'react';
import { Text, TextInput, PanResponder, Animated } from 'react-native';
import styles from '../styles/MainStyles'

class Task extends React.Component {  

  constructor(props) {
    super(props)
    const x = new Animated.Value(0)
    let editMode = false
    let checked = false
    const name = this.props.item.name
    let longPress= ''

    const panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderGrant: (event, gesture) => {
        longPress = setTimeout(() => {
          if(x._value === 0){
            this.setState({ editMode: true, checked: false })
          }}, 500)
      },
      onPanResponderMove: (event, gesture) => {
        x.setValue(gesture.dx)
      },
      onPanResponderRelease:  (event, gesture) => {
        clearTimeout(longPress)
        if(gesture.dx === 0){
          if(this.state.editMode === false){
            this.setState({checked: !this.state.checked})
            this.props.editTask(this.props.item.key, this.state.name, !this.state.checked)
          }
        }
        if(gesture.vx > 1 || gesture.vx < -1){
          Animated.timing(x, {
            toValue: gesture.vx * 300,
            duration: 200,
            useNativeDriver: true
          }).start(() => this.props.deleteTask(this.props.item.key))
        }
        else{
          Animated.timing(x, {
            toValue: 0,
            duration: 200,
            useNativeDriver: true
          }).start()
        }
      }
   });
   this.state = { panResponder, x, editMode, checked, name, checked: this.props.checked }
}

    render() {
      let handles = this.state.panResponder.panHandlers
      return(
        <Animated.View style={[styles.card, { translateX: this.state.x }, this.state.checked ? styles.checked : {}]} {...handles}>
          { this.state.editMode ?
          <TextInput
            style={styles.taskText}
            value={this.state.name}
            onChangeText={(value) => {
              this.setState({ name: value })
              this.props.editTask(this.props.item.key, value, this.state.checked)
              }}
            autoFocus
            onBlur={() => this.setState({ editMode: false })} />:
          <Text style={styles.taskText}>{this.state.name}</Text>}
        </Animated.View>
      )
    }
  }

  export default Task